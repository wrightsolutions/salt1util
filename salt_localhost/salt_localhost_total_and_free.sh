#!/bin/sh
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Show Python version and memory summary for the local machine

# Masterless: /usr/bin/salt-call --local grains.item mem_total

SALTBIN_=/usr/bin/salt
#printf "$SALTBIN_\n"
HOSTNAME_=$(/bin/hostname --long)
#printf "System running Python version "
#${SALTBIN_} ${HOSTNAME_} grains.item pythonversion sanitize=True
${SALTBIN_} ${HOSTNAME_} grains.item pythonversion
${SALTBIN_} ${HOSTNAME_} grains.item mem_total
# No such grain: ${SALTBIN_} ${HOSTNAME_} grains.item mem_free 
printf "Total memory (above) and current usage shown below\n"
# ${SALTBIN_} ${HOSTNAME_} status.meminfo
# status.meminfo produces much output and is great if you want to parse detailed memory stats
# but we are just interested in short summary so cmd.run a binary instead
${SALTBIN_} ${HOSTNAME_} cmd.run '/usr/bin/free -m'
