#!/bin/sh
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Run a single sls file through salt in TEST MODE
# Argument to this script should be the name of the single sls file you wish to execute
# Example1: salt_localhost_test_single.sh minimal
# Example2: salt_localhost_test_single.sh apache2

# Masterless: /usr/bin/salt-call --local state.sls apache2 test=True

# Put test=True as last item in the command for best results
# otherwise you might see response "test=True" is not available
SALTBIN_=/usr/bin/salt
#printf "$SALTBIN_\n"
HOSTNAME_=$(/bin/hostname --long)
printf "${SALTBIN_} ${HOSTNAME_} state.sls $* test=True\n"
${SALTBIN_} ${HOSTNAME_} state.sls $* test=True
