#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2016, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

""" Script to create a file.managed section of an input configuration file

First argument is section_label and will be the name that is given to the section in salt

./salt_file_managed.py 'collectd_thresholds' thresholds.conf
"""

import fileinput
import re
#from os import path,sep
from sys import argv
#from string import printable
prog = argv[0].strip()

SPACES2 = chr(32)*2
SPACES6 = chr(32)*6
SPACES8 = chr(32)*8


TEMPLATE_BEFORE="""%s:
  file.managed:
  - name: /tmp/%s
  - contents: |"""


def content_indented(fileinput_given):
    lines_indented = []
    for line in fileinput.input(fileinput_given):
        lines_indented.append("%s%s" % (SPACES6,line))
    return lines_indented


if __name__ == '__main__':
    exit_rc = 0

    section_label = None
    
    if len(argv) > 1:
        section_label = argv[1]
    else:
        section_label = 'salt_managed1'

    fileinput_given = argv[2:]
    section_lines = content_indented(fileinput_given)
    
    if len(section_lines) < 1:
        exit_rc = 151
    else:
        section_label_stripped = section_label.strip()
        managed_filename = fileinput_given[0]
        if fileinput_given[0].startswith(chr(47)):
            managed_filename = managed_filename.lstrip(chr(47))
        print TEMPLATE_BEFORE % (section_label_stripped,managed_filename)
        for line in section_lines:
            print line,
        print "%s- backup: minion" % SPACES2
    exit(exit_rc)

