#!/usr/bin/env python

from hashlib import sha1
from sys import argv
from string import printable

SET_PRINTABLE=set(printable)
COLON=chr(58)

def process_yaml(fpath=None):
	hash_list = []
	with open(fpath) as yamf:
		translate_delete = ''.join(map(chr, range(0,9)))
		#print(len(translate_delete))
		for linenum,line in enumerate(yamf.readlines()):
			#print("processing line %d" % linenum)
			if set(line).issubset(SET_PRINTABLE):
				pass
			else:
				print('line %s has one or more unprintables!' % linenum)
			line_translated = line.translate(None,translate_delete)
			if len(line) != len(line_translated):
				print('line %s has tab or other low range chr()' % linenum)
				continue
			cpos = line.index(COLON)
			if cpos < 3:
				continue
			after_stripped = line[cpos:].strip()
			if len(after_stripped) > 1:
				hash_list.append(sha1(after_stripped))
	checksum_list = [ h.hexdigest() for h in hash_list ]	
	return checksum_list


if __name__ == "__main__":
	path_given = None
	if len(argv) > 2:
		exit(110)
	else:
		path_given = argv[1]
	checksum_list = process_yaml(path_given)
	for csum in checksum_list:
		print(csum)


