#!/usr/bin/env python

from sys import argv
from string import printable

SET_PRINTABLE=set(printable)

def process_yaml(fpath=None):
	with open(fpath) as yamf:
                translate_delete = ''.join(map(chr, range(0,9)))
                #print(len(translate_delete))
		for linenum,line in enumerate(yamf.readlines()):
                        #print("processing line %d" % linenum)
                        if set(line).issubset(SET_PRINTABLE):
                                pass
                        else:
                                print('line %s has one or more unprintables!' % linenum)
                        line_translated = line.translate(None,translate_delete)
                        if len(line) == len(line_translated):
                                continue
                        print('line %s has tab or other low range chr()' % linenum)



if __name__ == "__main__":
	path_given = None
	if len(argv) > 2:
		exit(110)
	else:
		path_given = argv[1]
	process_yaml(path_given)
